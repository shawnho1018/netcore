#!/bin/bash
export PROJECT_ID="anthos-demo-280104"
export KEY="/Users/shawnho/.ssh/id_rsa.sme"
export GCLOUD_ACCOUNT="shawnho@google.com"
export REPO_URL="ssh://${GCLOUD_ACCOUNT}@source.developers.google.com:2022/p/${PROJECT_ID}/r/config-repo"
export CLUSTER="gcptaipei"

gsutil cp gs://config-management-release/released/latest/config-management-operator.yaml config-management-operator.yaml

kubectl apply -f config-management-operator.yaml

kubectl create secret generic git-creds \
--namespace=config-management-system \
--from-file=ssh=$KEY

cat ./config-sync.yaml | \
  sed 's|<REPO_URL>|'"$REPO_URL"'|g' | \
  sed 's|<CLUSTER_NAME>|'"$CLUSTER"'|g' | \
  sed 's|none|ssh|g' | \
  kubectl apply -f -

