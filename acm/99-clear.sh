#!/bin/bash

kubectl delete -f config-sync.yaml
kubectl delete secret git-creds -n config-management-system
kubectl delete -f config-management-operator.yaml
